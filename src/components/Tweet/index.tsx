import React from "react";

import {
	Container,
	Retweeted,
	Body,
	Avatar,
	Content,
	Header,
	Description,
	Dot,
	ImageContent,
	Icons,
	Status,
	RocketseatIcon,
	CommentIcon,
	LikeIcon,
	ReweetIcon,
} from "./styles";

const Tweet: React.FC = () => {
	return (
		<Container>
			<Retweeted>
				<RocketseatIcon />
				Voce retweetou
			</Retweeted>

			<Body>
				<Avatar />

				<Content>
					<Header>
						<strong>Rocketseat</strong>
						<span>@rocketseat</span>
						<Dot />
						<time>27 de jul</time>
					</Header>

					<Description>Foguete nao tem re</Description>
					<ImageContent />

					<Icons>
						<Status>
							<CommentIcon /> 18
						</Status>
						<Status>
							<ReweetIcon /> 7
						</Status>
						<Status>
							<LikeIcon /> 188
						</Status>
					</Icons>
				</Content>
			</Body>
		</Container>
	);
};

export default Tweet;
