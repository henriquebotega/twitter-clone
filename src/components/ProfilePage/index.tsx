import React from "react";

import {
	Container,
	Banner,
	Avatar,
	ProfileData,
	LocationIcon,
	CakeIcon,
	Followage,
	EditButton,
} from "./styles";
import Feed from "../Feed";

const ProfilePage: React.FC = () => {
	return (
		<Container>
			<Banner>
				<Avatar />
			</Banner>

			<ProfileData>
				<EditButton outlined>Editar perfil</EditButton>
				<h1>Guilherme Rodz</h1>
				<h2>@guilherme_rodz</h2>

				<p>
					Desenvolvido por <a href="#">Rocketseat</a>
				</p>

				<ul>
					<li>
						<LocationIcon />
						Sao Paulo, Brasil
					</li>
					<li>
						<CakeIcon />
						Nascido em 20 de Agosto de 1984
					</li>
				</ul>

				<Followage>
					<span>
						seguindo <strong>94</strong>
					</span>
					<span>
						<strong>941 </strong> seguidores
					</span>
				</Followage>
			</ProfileData>

			<Feed />
		</Container>
	);
};

export default ProfilePage;
